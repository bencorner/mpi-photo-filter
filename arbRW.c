




















#include <stdlib.h>
#include "mpi.h"
#include <stdio.h>
#include <curses.h>      /*  Will hopefully be used to make pretty LIFE output w/ refreshing buffer instead of scrolling  */

#define SIZE 16


/* #define UP    0 */          // These seem to work fine
/* #define UPRIGHT    1 */
/* #define RIGHT  2 */
/* #define DOWNRIGHT    3 */
/* #define UPLEFT    4 */
/* #define LEFT  5 */
/* #define DOWNLEFT 6 */
/* #define DOWN    7 */


#define UPLEFT    0
#define UP    1       
#define UPRIGHT    2
#define RIGHT  3
#define LEFT  4
#define DOWNLEFT 5
#define DOWN    6
#define DOWNRIGHT    7

//tags
#define ODD   111
#define EVEN  222

void printLife(int * cells);
int *readStateFromFile(char *filename, int *w, int *h, int *x);

//////////////////////////////////////////
//////////////////////////////////////////


int *readStateFromFile(char *filename,int *w, int *h, int *x) {

      #define BIGENOUGH 128

      int width, height, max;

      char type[BIGENOUGH];
      char comment[BIGENOUGH];

      //
      // attempt to open the file
      FILE *f = fopen(filename,"r");
      if (!f) {
	    fprintf(stderr,"File '%s' failed to open.\n", filename);
	    return NULL;
      }

      //
      // read header info
      fscanf(f,"%s\n",type);
      printf("type: %s\n",type);

      /* if (!strcmp(type,"P2")) { */
      /* 	    fprintf(stderr,"File '%s' does not appear to be a PGM in P2 format.\n", filename); */
      /* 	    return NULL; */
      /* } */

      fscanf(f,"%s\n",comment);
      while (fgetc(f) != '\n');
//      printf("comment: %s\n",comment);
      fscanf(f,"%d %d\n",&width,&height);
      fscanf(f,"%d\n",&max);
      printf("%d x %d : %d\n",width,height,max);

      //
      // read pixel values
      int *image = malloc(sizeof(int)*width*height);
      int i;
      for (i=0; i<width*height; i++) {
	    fscanf(f,"%d",&image[i]);
      }

      fclose(f);
      *w = width;
      *h = height;
      *x = max;
      return image;
}







int *writeStateToFile(char *filename,int *w, int *h, int *x) {

      #define BIGENOUGH 128

      int width, height, max;

      char type[BIGENOUGH];
      char comment[BIGENOUGH];

      //
      // attempt to open the file
      FILE *f = fopen(filename,"r");
      if (!f) {
	    fprintf(stderr,"File '%s' failed to open.\n", filename);
	    return NULL;
      }

      //
      // read header info
      fscanf(f,"%s\n",type);
      printf("type: %s\n",type);

      /* if (!strcmp(type,"P2")) { */
      /* 	    fprintf(stderr,"File '%s' does not appear to be a PGM in P2 format.\n", filename); */
      /* 	    return NULL; */
      /* } */

      fscanf(f,"%s\n",comment);
      while (fgetc(f) != '\n');
//      printf("comment: %s\n",comment);
      fscanf(f,"%d %d\n",&width,&height);
      fscanf(f,"%d\n",&max);
      printf("%d x %d : %d\n",width,height,max);

      //
      // read pixel values
      int *image = malloc(sizeof(int)*width*height);
      int i;
      for (i=0; i<width*height; i++) {
	    fscanf(f,"%d",&image[i]);
      }

      fclose(f);
      *w = width;
      *h = height;
      *x = max;
      return image;
}










      //
      // Print lifestate
void printLife(int *cells) {
      int i,j;
      
      printf("\n\n\n");
      for ( i = 0; i < 6; i++) {
	    printf("\n Row %d:   ", i);
	    for (j = 0; j < 6; j++) {
		  printf("%d ",cells[6*i + j]);
		  
	    }
	    
      }
      printf("\n There we go! \n \n");
      
}


//// FOR 6x6-SIZED LIFE:
//      - changed           dims{4,4}   ->   dims{6,6}
//      - added             lifestate[36]
//      - changed           reqs, stats to size 36 (?)


int main(argc,argv)
int argc;
char *argv[];  {
      int  numtasks, rank, source, dest, outbuf, i, tag=0, 
	    inbuf[8]={0,0,0,0,0,0,0,0}, rankbuf[8]={0,0,0,0,0,0,0,0}, 
	    inbufSum, nbrs[8], dims[2]={6,6}, periods[2]={0,0}, reorder=0, coords[2];  
	    int * lifebit, shittyCounter = 0;
	    int ierr;

	    MPI_Request reqs[36];
	    MPI_Status stats[36];
	    MPI_Comm cartcomm;

	    int lifewidth, lifeheight,lifemax;
	    int *lifestate = readStateFromFile("input.pgm",&lifewidth,&lifeheight,&lifemax);
	    printLife(lifestate);
	    
	    
	    /* int lifestate[36] = {0,1,0,0,0,0, */
	    /* 			 0,0,1,0,0,0, */
	    /* 			 1,1,1,0,0,0, */
	    /* 			 0,0,0,0,0,0, */
	    /* 			 0,0,0,0,0,0, */
	    /* 			 0,0,0,0,0,0} */
	    
	    MPI_Init(&argc,&argv);
	    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	    
	    
	    printf("current value of numtasks:     %d \n",numtasks);


		  /// This code segment populates each node's nbrs[] 
		  MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, reorder, &cartcomm);
		  MPI_Comm_rank(cartcomm, &rank);
		  MPI_Cart_coords(cartcomm, rank, 2, coords);
		  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

		  MPI_Cart_shift(cartcomm, 0, 1, &nbrs[UP], &nbrs[DOWN]);
		  MPI_Cart_shift(cartcomm, 1, 1, &nbrs[LEFT], &nbrs[RIGHT]);






		  // Send-Recv your right,left processor-names for upright,upleft proc-names
		  
		  ierr = MPI_Sendrecv(&nbrs[RIGHT], 2, MPI_INT, nbrs[UP], tag, 
				      &rankbuf[0], 2, MPI_INT, nbrs[DOWN], tag, MPI_COMM_WORLD, stats);
		  nbrs[DOWNRIGHT] = rankbuf[0];
		  nbrs[DOWNLEFT] = rankbuf[1];


		  
		  ierr = MPI_Sendrecv(&nbrs[RIGHT], 2, MPI_INT, nbrs[DOWN], tag, 
				      &rankbuf[2], 2, MPI_INT, nbrs[UP], tag, MPI_COMM_WORLD, stats);
		  
		  nbrs[UPRIGHT] = rankbuf[2];
		  nbrs[UPLEFT] = rankbuf[3];

		  
			
		  lifebit = &(lifestate[rank]);  // 
		  outbuf = (lifestate[rank]);    // to send the current value of this processor
		  
		  // Life computations
		  while (1==1) {

			if (rank == 0) {
			      printf("\n\n Hello, this is the lifeloop! \n");
			      printLife(lifestate);
			}
			     
			inbufSum = *lifebit;         // Factoring the current pixel-value into the final average value for this block of pixels.
			//  cells give/receive their values to their neighbours
			for (i=0; i<8; i++) {
			      
			      dest = nbrs[i];
			      source = nbrs[7-i];


			      if ((source >= 0) && (source < 36) ){
				    ierr = MPI_Irecv(&(inbuf[i]), 1, MPI_INT, source, tag,
						     MPI_COMM_WORLD, &(reqs[rank]));
			      }

			      if ((dest >= 0)  && (dest < 36) ){
				    ierr = MPI_Send(lifebit, 1, MPI_INT, dest, tag,   
						    MPI_COMM_WORLD);                 //Trying blocking-Send for each node,
			      }


			      MPI_Barrier(MPI_COMM_WORLD);
			      inbufSum = inbufSum + inbuf[i]; 
			}
			
			
			MPI_Barrier(MPI_COMM_WORLD);			

			*lifebit = (inbufSum / 8 );      ///////////   Will need to modify this for actual image processing
							 ///////////   to account for edge-pixels
			
	       ////// This switch statement is used for running the Game Of Life simulation. */
			/* switch (inbufSum) */
			/* { */
			      
			/* case 3:                      // Cell will be ALIVE if it has precisely 3 living neighbours. */
			/*       *lifebit = 1; */
			/*       break; */

			/* case 2:                      // Cell-state is not changed if it has precisely 2 living neighbours. */
			/*       break; */

			/* default:                     // Cell will end up being DEAD if it doesn't fit the previous cases. */
			/*       *lifebit = 0; */
			/* } */
			
			
			MPI_Barrier(MPI_COMM_WORLD);			
			// Process[0] collects all the data. 
			MPI_Gather(lifebit,1,MPI_INT, lifestate,1,MPI_INT,0,MPI_COMM_WORLD);  

		  }
		  
		  
	    MPI_Finalize();
	    
} 

int simplemain(int argc, char **argv) {
      int lifewidth, lifeheight,lifemax;
      int *lifestate = readStateFromFile("input.pgm",&lifewidth,&lifeheight,&lifemax);
      printLife(lifestate);
}
